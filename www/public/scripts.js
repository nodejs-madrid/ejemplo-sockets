$marcaBase = $(".marca.base");

let socket = io();

socket.on('clicado', coordenadas => colocaMarca(coordenadas));

function colocaMarca(coordenadas) {
    let $marca = $marcaBase.clone();
    $marca.appendTo($("body"));
    $marca.css({ top: coordenadas.top, left: coordenadas.left, opacity: 1 });
    setTimeout(() => {
        $marca.css({ opacity: 0 });
        $marca.remove();
    }, 500);
}

$(document).on('click', e => {
    socket.emit('click', { top: e.clientY, left: e.clientX });
})