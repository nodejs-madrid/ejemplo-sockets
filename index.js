const http = require('http');
const express = require('express');

const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);

app.use(express.static(__dirname + '/www/public/'));
app.get('/', (req, res) => {
    res.sendFile(__dirname + '/www/index.html');
})

server.listen(8080, () => console.log("UP"));

io.on('connection', function (socket) {
    console.log('a user connected');
});
io.on('connection', socket => {
    console.log("Usuario conectado");
    socket.on("disconnect", () => {
        console.log("Usuario desconectado");
    })
    socket.on("click", coordenadas => {
        socket.broadcast.emit("clicado", coordenadas);
    });

});